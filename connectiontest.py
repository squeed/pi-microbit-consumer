import time

from plotly.offline import plot
import plotly.graph_objs as go

from bluezero import microbit
from bluezero import async_tools


print('Bluetooth setup')
ubit = microbit.Microbit(adapter_addr='B8:27:EB:85:73:BA',
                         device_addr='CE:26:B2:7A:8A:99',
                         accelerometer_service=False,
                         button_service=True,
                         led_service=False,
                         magnetometer_service=False,
                         pin_service=False,
                         temperature_service=False,
                         uart_service=True)
looping = True
print('Trying to connect...')
ubit.connect()
print('Connected')
ubit.set_pin(0, True, True)

data_x = []
data_y = []
tick = 0

eloop = async_tools.EventLoop()


def ping():
    ubit.uart = 'ping#'
    return True


def goodbye():
    ubit.quit_async()
    ubit.disconnect()
    return False


ubit.subscribe_uart(print)
eloop.add_timer(10000, ping)
eloop.add_timer(30000, goodbye)

ubit.disconnect()