import json
import time

import websockets as websockets
from bluezero import microbit
import asyncio

def connectBluetooth():
    print('Bluetooth setup')
    ubit = microbit.Microbit(adapter_addr='B8:27:EB:85:73:BA',
                             device_addr='CE:26:B2:7A:8A:99',
                             accelerometer_service=False,
                             button_service=True,
                             led_service=False,
                             magnetometer_service=False,
                             pin_service=True,
                             temperature_service=False)
    print('Trying to connect...')
    ubit.connect()
    print('Connected')
    ubit.set_pin(0, True, True)
    return ubit

ubit = connectBluetooth()
name = "Plant 1"

async def run():
    uri = "ws://cc852ef3.ngrok.io/send"
    async with websockets.connect(uri) as websocket:
        while True:
            val = str(ubit.pin_values.get('0'))
            if val is not '':
                print('Got value ', val)
                print('Sending value to db...')
                data = {'name': name, 'value': val}
                json_data = json.dumps(data)
                await websocket.send(json_data)
                time.sleep(1)

asyncio.run(run())
